<?php

class Card {
    public $name;
    public $effect;
    public $drawed = 0;
    
    public function __construct($name,$effect){
        $this->name = $name;
        $this->effect = $effect;
    }
    
    public function toString(){
        return "Name: " .$this->name. ", effect: " .$this->effect.".<br>";
    }
}