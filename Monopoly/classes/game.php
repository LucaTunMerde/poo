<?php

class Game{
    
    public $win;
    public $players;
    public $moneyLeaderboard;
    public $proprietiesLeaderboard;
    public $winner;
    
    public function __construct($players){
        $win = false;
        $this->players = $players;
        $this->moneyLeaderboard = [];
        $this->proprietiesLeaderboard = [];
        $this->winner = null;
        //echo("Game started");
    }
    
    
    //Checks if the game is finished, if so it will stop it
    public function isWin(){
        $bankruptPlayers = 0;
        $winner = null;
        foreach($this->players as $player){
            //We check every player if they are bankrupt
            if($player->money <=0 && !count($player->properties)){
                $bankruptPlayers++;
            }else{
                $winner = $player;
            }
        }
        
        //If there is only one player left, not in bankrupt the game is finished
        if(count($this->players)-1 == $bankruptPlayers){
            $this->win = true;
            $this->winner = $winner;
        }
    }
    
    
    //This update the multiple leaderboards based on the players ressourses 
    public function updateLeaderboard(){
        $m = $p = array();

        foreach($this->players as $player){
            $m[$player->money] = $player;
            $p[count($player->properties)] = $player;
        }
        
        krsort($m);
        krsort($p);
        
        $this->moneyLeaderboard = $m;
        $this->proprietiesLeaderboard = $p;
    }
    
    
    public function displayEnd(){
        echo("Congrats the game is finished, ".$this->winner->name." won!<br>");
        
        
        $str = "<table>
                    <tr>
                        <tr>Money</tr>
                        <tr>Properties</tr>
                        <tr>Houses & Hotels</tr>
                    </tr>";
        foreach($this->players as $player){
            $nbHouses = 0;
            
            //var_dump($player);
            foreach($player->properties as $cityGroups){
                
                foreach($cityGroups as $pro){
                    if($pro instanceof Street){
                        $nbHouses += $pro->house;
                    }
                }
            }
            
            $str .= "<tr>
                        <td>".$player->money."</td>
                        <td>".count($player->properties)."</td>
                        <td>".$nbHouses."</td>
                    </tr>";
        }
                    
        
        $str .= "</table>";
        
        echo $str;
    }
    
    
}