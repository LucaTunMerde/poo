<?php

class Player{
    
    public static $cardDeck;
    public $name;
    public $money;
    public $properties;
    public $cards;
    public $lastRolledDice = [0,0];
    public $position = 0;
    public $doubleCounter = 0;

    public function __construct($name, $money, $properties, $cards){
        $this->name = $name;
        $this->money = $money;
        $this->properties = $properties;
        $this->cards = $cards;
    }
    
    
    
    /* METHODS */
    
    
    
    //Returns a number between 1 and 6 to simulate the roll of a dice
    public function throwDice(){
        
        $dice1 = rand(1,6);
        $dice2 = rand(1,6);
        $this->lastRolledDice = [$dice1,$dice2];
        
        //If we break a double dice combo the counter goes back to 0
        if($this->doubleCounter && $dice1 != $dice2){
            $this->doubleCounter = 0;
        }
        //If we make a double we increment the counter
        if($dice1 == $dice2){
            $this->doubleCounter++;
        }
        //If the player makes 3 times double in a row he goes to prison
        if($this->doubleCounter == 3){
            $this->moveTo(10,true);
            echo("Player goes to jail");
            return;
        }
        
        return [$dice1,$dice2];
    }
    
    //Moves the player based on his roll dice
    public function move($array){
        $nb = ($array[0] + $array[1] );
        if($this->position + $nb> 39){
            $diff = 40 - ($this->position + $nb);
            
            $this->position = - $diff;
            
            
            $this->money += 200;
            echo("Player has passed start, they recieve 200");
        }else{
            $this->position += $nb;
        }
    }
    
    
    //Moves the player to the specified location
    public function moveTo($index,$prison){
        //If $prison is true the player does not receive any money 
        //If the player goes through the start while moving
        if(!prison && $this->position > $index){
            $this->money += 200;
        }
        $this->position = $index;
    }
    
    
    
    //Buys a property, adds it into the player properties list
    public function buyProperty($property){
        //If the supplented variable is a property
        if($property instanceof Property){
            //If the property ain't bought yet and ain't mortgaged yet
            if(!$property->bought && !$property->mortgaged){
                //If player has enough money to buy it
                if($this->money >= $property->price){
                    //Adds it into the player properties list 
                    
                    $this->money -= $property->price;
                    $property->bought = 1;
                    
                    $this->properties[$property->cityGroup][]= $property;
                }else{
                    echo("The player does not have enough money <br>");
                }  
            }else{
                echo("The property is already bought <br>");
            }
        }else{
            echo("This is not a property <br>");
        }
    }
    
    
    //Pays the rent to another player based on a specified property owned by this one
    public function payRent($player,$property){
        //If the submited values are of the good type
        if($player instanceof Player && $property instanceof Property){
            //If the player has the property
            if(in_array($property,$player->properties[$property->cityGroup])){
                //If the property is not morgaged 
                if(!$property->mortgaged){
                    //Transfers the money from one player to the other based on the type of property
                    switch(get_class($property)){
                        case "Street" :
                            $this->money -= $property->rent;
                            $player->money += $property->rent;
                            break;
                        case "Railroad":
                            $nbRailroad = count($player->properties[$property->cityGroup]);
                            $cost = ($nbRailroad == 1) ? 25 : 25 * 2 * ($nbRailroad-1);
                            $this->money -= $cost;
                            $player->money += $cost;
                            break;
                        case "Service":
                            $cost = (count($player->properties[$property->cityGroup])==1) ? $this->lastRolledDice * 4 : $this->lastRolledDice * 10;
                            $this->money -= $cost;
                            $player->money += $cost;
                            break;
                        default:
                            echo("Cette propriété n'a pas de loyer ou une erreur est survenue");
                    }
                }else{
                    echo("This property is mortgaged you do not have to pay a rent <br>");
                }
            }else{
                echo("This player does not possess this property <br>");
            }
        }else{
           echo("The submited values do not match expected classes <br>"); 
        }
    }
    
    
    //Allows the player to mortgage one of his properties
    public function mortgage($property){
        //If the supplented variable is a property
        if($property instanceof Property){
            //If the submited property is not mortgaged yet
            if(!$property->mortgaged){
                //If the user posesses the property
                if(in_array($property,$this->properties[$property->cityGroup])){
                    //Adds the mortgage value to player's money
                    //Sets the property on morgaged
                    $this->money += $property->mortgagePrice;
                    $property->mortgaged = 1;
                }else{
                    echo("This property does not belong to you <br>");
                }
            }else{
                echo("This property is already mortgaged <br>");
            }
        }else{
            echo("The submited variable is not a Property <br>");
        }
    }
    
    //Allows the player to unmortgage one of his mortgaged properties
    public function unmortgage($property){
        //If the supplented variable is a property
        if($property instanceof Property){
            //If the submited property is mortgaged
            if($property->mortgaged){
                //If the user posesses the property
                if(in_array($property,$this->properties[$property->cityGroup])){
                    //Adds the mortgage value to player's money
                    //Sets the property on morgaged
                    $this->money -= $property->mortgagePrice + ($property->mortgagePrice * 10/100) ;
                    $property->mortgaged = 0;
                }else{
                    echo("This property does not belong to you <br>");
                }
            }else{
                echo("This property is already mortgaged <br>");
            }
        }else{
            echo("The submited variable is not a Property <br>");
        }
    }
    
    
    //Draws a card from the card deck
    public function drawCard(){
        $cardDrawed = 1;
        $cardNumber = count(self::$cardDeck);
        
        //While a card has not been drawed
        while($cardDrawed){
            //Gets a random card
            $cardToDraw = rand(0,$cardNumber);
            //If the selected card has not been already drawn and is set
            if(isset(self::$cardDeck[$cardToDraw])){
                if(!self::$cardDeck[$cardToDraw]->drawed){
                    //Adds the card to the player's card list 
                    $this->cards[] = self::$cardDeck[$cardToDraw];
                    self::$cardDeck[$cardToDraw]->drawed = 1;
                    $cardDrawed = 0;
                }
            }
        }
    }
    
    
    //Sets the card deck from which the players will draw cards
    public static function setCardDeck($cardArray){
        self::$cardDeck = $cardArray;
        //No cheating ! We shuffle the cards :)
        shuffle(self::$cardDeck);
    }
    
    
    
    //Allows the player to buy a house on a property when all conditions are good
    public function buyHouse($property){
        //If the submited variable is a property
        if($property instanceof Property){
            //If the player has this property
           if(in_array($property,$this->properties[$property->cityGroup])){
               //If the player has all properties from it's group
               if(
                   (
                       count($this->properties[$property->cityGroup])==3
                   ) 
                   ||
                   (
                       (
                           count($this->properties[$property->cityGroup])==2 
                           && 
                           in_array($property->cityGroup,[0,7])
                       )
                   ) 
               ){  
                   //We get the number of houses on the city that has the least
                   $minHouse = 5;
                   $mortgaged = 0;
                   foreach($this->properties[$property->cityGroup] as $pro){
                       if($pro->house < $minHouse){
                           $minHouse = $pro->house;
                       }
                       if($pro->mortgaged){
                           $mortgaged = 1;
                       }
                   }
                   
                   //None of the properties are mortgaged
                   if(!$mortgaged){
                       //If the city has less house that the other ones
                       if($property->house <= $minHouse && $property->house != 5){
                           if($property->house == 4){
                               if($this->money >= $property->hotelPrice){
                                   $this->money -= $property->hotelPrice;
                                   $property->house ++;
                                   $property->updateRentPrice();
                               }else{
                                   echo("The player does not have enough money to build a hotel <br>");
                               }
                           }else{
                               //If the player has enough money to buy a house
                               if($this->money >= $property->housePrice){
                                   $this->money -= $property->housePrice;
                                   $property->house ++;
                                   $property->updateRentPrice();
                               }else{
                                   echo("The player does not have enough money to build a house <br>");
                               }
                           }
                       }elseif($property->house == 5){
                           echo("This property already has an hotel, you can't build more <br>");
                       }else{
                           echo("The player needs to build house on the other cities from the group <br>");
                       }
                   }else{
                       echo("One city of this city group is mortgaged <br>");
                   }
               }else{
                   echo("The player does not own every properties from the group <br>");
               }
           }else{
               echo("The player does not own this property <br>");
           } 
        }else{
            echo("This is not a property <br>");
        }
    }
    
    
    //Sells a house or a hotel to get money
    public function sellHouse($property){
        //If the submited variable is a property
        if($property instanceof Property){
            //If the player has this property
           if(in_array($property,$this->properties[$property->cityGroup])){
               //If this property has a house to sell
               if($property->house){
                   //We get the number of houses on the city that has the most
                   $maxHouse = 0;
                   foreach($this->properties[$property->cityGroup] as $pro){
                       if($pro->house > $maxHouse){
                           $maxHouse = $pro->house;
                       }
                   }
                   if($property->house >= $maxHouse){ 
                       $this->money += round($property->rentPrices[$property->house] / 2);
                       $property->house --;
                       $property->updateRentPrice();
                   }else{
                       echo("You cannot sell here. <br>");
                   }
               }
           }else{
               echo("This property does not belong to you <br>");
           }
        }else{
            echo("This is not a property <br>");
        }
    }
        
    
    
    
    

    public function toString(){
        $str = "Name: " .$this->name. ", money: " .$this->money. ", properties: <br>";
        
        if(count($this->properties)){
            foreach($this->properties as $prop){
                $str .= $prop->toString();
                $str .=", ";
            }
        }else{
            $str .= "none <br>, ";
        }
        
        $str .= "cards: <br>" ;
        
        if(count($this->cards)){
            foreach($this->cards as $card){
                $str .= $card->toString();
            }
        }else{
            $str .= "none.";
        }
        
        return $str . "<br>";
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
?>