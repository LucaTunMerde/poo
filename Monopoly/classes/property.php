<?php
include_once("square.php");

class Property extends Square{
    
    public $price;
    public $rent;
    public $bought = 0;
    public $mortgagePrice;
    public $mortgaged = 0;
    
    public function __construct($name,$price,$mortgagePrice,$cityGroup){
        parent::__construct($name,$cityGroup,0,0);
        $this->price = $price;
        $this->mortgagePrice = $mortgagePrice;
    }
        
    
    public function toString(){
        return "Name: " .$this->name. ", price: " .$this->price. ", bought: " . $this->bought. ".<br>";
    }
}