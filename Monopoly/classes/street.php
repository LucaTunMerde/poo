<?php

include_once("property.php");

class Street extends Property{
    
    public $house = 0;
    public $housePrice;
    public $hotelPrice;
    public $rentPrices;
    
    public function __construct($name,$price,$mortgagePrice,$cityGroup,$housePrice,$hotelPrice,$rentPrices){
        parent::__construct($name,$price,$mortgagePrice,$cityGroup);
        $this->rent = $rentPrices[0];
        $this->housePrice = $housePrice;
        $this->hotelPrice = $hotelPrice;
        $this->rentPrices = $rentPrices;
    }
    
    public function updateRentPrice(){
        $rentPercentage = $this->house;
        $this->rent = $this->rentPrices[$rentPercentage];
    }
    
    
    public function toString(){
        return "Name: " .$this->name. ", price: " .$this->price. ", bought: " . $this->bought. ".<br>";
    }
}