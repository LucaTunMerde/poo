<?php
include_once("classes/player.php");
include_once("classes/property.php");
include_once("classes/street.php");
include_once("classes/railroad.php");
include_once("classes/service.php");
include_once("classes/card.php");
include_once("classes/game.php");
include_once("variables_setup.php");

/* GAME SETUP */

//Sets the board
$board = array();

$squarename = "case00";
for($i=0;$i<40;$i++){
    $board[]= $$squarename;
    $squarename++;
}
unset($squarename);

//List of players
$players = array($j,$j2);


//List of cards 
$cards = array();

$cardname = "carte00";
for($i=0;$i<2;$i++){
    $cards[]= $$cardname;
    $cardname++;
}
unset($cardname);

//The card deck from which players will draw
Player::setCardDeck($cards);
unset($cardname);



//Boolean to know if the game is won
$win = false;


/* GAME */

$game = new Game($players);

while(!$win){
    //Throwing dice
    $dice = $j->throwDice();
    echo("Player 1 is throwing dice : ". $dice[0] .",".$dice[1] . ".<br>");



    echo("Player 1 buy properties. <br>");
    $j->buyProperty($case01);
    $j->buyProperty($case03);
    $j->buyProperty($case05);
    $j->buyProperty($case25);



    echo("Player 2 pays rent to player 1. <br>");
    $j2->payRent($j,$case05);


    echo("Player 1 mortgages his property. <br>");
    $j->mortgage($case01);
    $j->unmortgage($case01);


    echo("Player 2 draws two cards. <br>");
    $j2->drawCard();
    $j2->drawCard();


    echo("Player 1 buys a house. <br>");
    $j->buyHouse($case01);
    $j->buyHouse($case03);
    $j->buyHouse($case01);
    $j->buyHouse($case03);
    $j->buyHouse($case01);

    echo("Player 1 sells a house. <br>");
    $j->sellHouse($case01);
    $j->sellHouse($case01);
    
    $j2->money = 0;
    $game->updateLeaderboard();
    $game->isWin();
    $win=true;
}
    
$game->displayEnd();

















?>








