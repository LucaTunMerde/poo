<?php

/*
City groups 
0-7 streets
8 Railroads
9 Services
10 Others (community,taxes,luck)
11 Not ownable Places (start,jail..)
*/



//Basic random rent values for each type of buildings (none,1,2,3,4,hotel)
$rentPrice = array(260,1300,3900,9000,11000,12750);


//Squares from the board
$case00 = new Square("Départ",11,0,0);
$case01 = new Street("Boulevard de Belleville",100,80,0,10,50,$rentPrice);
$case02 = new Square("Caisse de communauté",10,0,0);
$case03 = new Street("Rue Lecourbe",100,80,0,10,50,$rentPrice);
$case04 = new Square("Impots sur le revenu",10,0,0);
$case05 = new Railroad("Gare Montparnasse",200,150,8);
$case06 = new Street("Rue de Vaugiraud",100,80,1,10,50,$rentPrice);
$case07 = new Square("Chance",10,0,0);
$case08 = new Street("Rue de courcelles",100,80,1,10,50,$rentPrice);
$case09 = new Street("Avenue de la République",100,80,1,10,50,$rentPrice);
$case10 = new Square("Prison",11,0,0);
$case11 = new Street("Boulevard de la Villette",100,80,2,10,50,$rentPrice);
$case12 = new Service("Compagnie d'électicité",150,120,9);
$case13 = new Street("Avenue de Neuilly",140,120,2,10,50,$rentPrice);
$case14 = new Street("Rue de Paradis",140,120,2,10,50,$rentPrice);
$case15 = new Railroad("Gare de Lyon",200,120,8);
$case16 = new Street("Avenue Mozart",180,120,3,10,50,$rentPrice);
$case17 = new Square("Caisse de Communauté",10,0,0);
$case18 = new Street("Boulevard St-Michel",140,120,3,10,50,$rentPrice);
$case19 = new Street("Place Pigalle",140,120,3,10,50,$rentPrice);
$case20 = new Square("Park gratuit",11,0,0);
$case21 = new Street("Avenue Matignon",140,120,4,10,50,$rentPrice);
$case22 = new Square("Chance",10,0,0);
$case23 = new Street("Boulevard Malsherbes",140,120,4,10,50,$rentPrice);
$case24 = new Street("Avenue Henri-Martin",140,120,4,10,50,$rentPrice);
$case25 = new Railroad("Gare du Nord",140,120,8);
$case26 = new Street("Faubourg St-Honoré",140,120,5,10,50,$rentPrice);
$case27 = new Street("Place de la Bourse",140,120,5,10,50,$rentPrice);
$case28 = new Service("Compagnie d'eau",140,120,9);
$case29 = new Street("Rue la Fayette",140,120,5,10,50,$rentPrice);
$case30 = new Square("Allez en prison",11,0,0);
$case31 = new Street("Avenue de Breuteuil",140,120,6,10,50,$rentPrice);
$case32 = new Street("Avenue Fosch",140,120,6,10,50,$rentPrice);
$case33 = new Square("Caisse de communauté",10,0,0);
$case34 = new Street("Boulevard des Capucines",140,120,6,10,50,$rentPrice);
$case35 = new Railroad("Gare St-Lazare",140,120,8);
$case36 = new Square("Chance",10,0,0);
$case37 = new Street("Avenue des Champs-Élysées",140,120,7,10,50,$rentPrice);
$case38 = new Square("Taxe de luxe",10,0,0);
$case39 = new Street("Rue de la Paix",140,120,7,10,50,$rentPrice);

//Cards
$carte00 = new Card("Sortie de prison", "Vous pouvez utiliser cette carte à n'importe quel moment.");
$carte01 = new Card("Chance", "La banque a fait une erreur en votre faveur.<br>+50€");


//Players
$j = new Player("Jean-Eude",5000,[],[]);
$j2 = new Player("Jean-Émilien",500,[],[]);