# poo

Most parts of the code is commented and explained



Here is a list of everything in this code:

Classes :
    Property 
        Street
        Railroad
        Service
    Player
    Card
    Game


Methods : 
    Player :
        throwDice()
        move()
        moveTo()
        buyProperty()
        payRent()
        mortgage()
        unmorgage()
        drawCard()
        buyHouse()
        sellHouse()
        setCardDeck() 
        toString() TODO
    Street :
        updateRentPrice()
        toString() TODO
    Card :
        toString() TODO
    Game:
        isWin()
        updateLeaderboard()